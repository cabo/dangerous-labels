# Dangerous Labels in DNS and E-mail

This repository tracks the development of [draft-dkg-intarea-dangerous-labels](https://datatracker.ietf.org/doc/draft-dkg-intarea-dangerous-labels/).

You may also be interested in the [editor's copy](https://dkg.gitlab.io/dangerous-labels/).

You can also find [an issue-tracker](https://gitlab.com/dkg/dangerous-labels/-issues) and [a way to suggest changes](https://gitlab.com/dkg/dangerous-labels/-/issues).
